import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { Clinique } from './clinique.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<Clinique>;

@Injectable()
export class CliniqueService {

    private resourceUrl =  SERVER_API_URL + 'api/cliniques';

    constructor(private http: HttpClient) { }

    create(clinique: Clinique): Observable<EntityResponseType> {
        const copy = this.convert(clinique);
        return this.http.post<Clinique>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(clinique: Clinique): Observable<EntityResponseType> {
        const copy = this.convert(clinique);
        return this.http.put<Clinique>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<Clinique>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<Clinique[]>> {
        const options = createRequestOption(req);
        return this.http.get<Clinique[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Clinique[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: Clinique = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<Clinique[]>): HttpResponse<Clinique[]> {
        const jsonResponse: Clinique[] = res.body;
        const body: Clinique[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to Clinique.
     */
    private convertItemFromServer(clinique: Clinique): Clinique {
        const copy: Clinique = Object.assign({}, clinique);
        return copy;
    }

    /**
     * Convert a Clinique to a JSON which can be sent to the server.
     */
    private convert(clinique: Clinique): Clinique {
        const copy: Clinique = Object.assign({}, clinique);
        return copy;
    }
}
