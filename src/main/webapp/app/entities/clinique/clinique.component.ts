import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Clinique } from './clinique.model';
import { CliniqueService } from './clinique.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-clinique',
    templateUrl: './clinique.component.html'
})
export class CliniqueComponent implements OnInit, OnDestroy {
cliniques: Clinique[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private cliniqueService: CliniqueService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.cliniqueService.query().subscribe(
            (res: HttpResponse<Clinique[]>) => {
                this.cliniques = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInCliniques();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Clinique) {
        return item.id;
    }
    registerChangeInCliniques() {
        this.eventSubscriber = this.eventManager.subscribe('cliniqueListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
