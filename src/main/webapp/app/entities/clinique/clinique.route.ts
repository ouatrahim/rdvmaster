import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { CliniqueComponent } from './clinique.component';
import { CliniqueDetailComponent } from './clinique-detail.component';
import { CliniquePopupComponent } from './clinique-dialog.component';
import { CliniqueDeletePopupComponent } from './clinique-delete-dialog.component';

export const cliniqueRoute: Routes = [
    {
        path: 'clinique',
        component: CliniqueComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'rdvmasterApp.clinique.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'clinique/:id',
        component: CliniqueDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'rdvmasterApp.clinique.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const cliniquePopupRoute: Routes = [
    {
        path: 'clinique-new',
        component: CliniquePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'rdvmasterApp.clinique.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'clinique/:id/edit',
        component: CliniquePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'rdvmasterApp.clinique.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'clinique/:id/delete',
        component: CliniqueDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'rdvmasterApp.clinique.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
