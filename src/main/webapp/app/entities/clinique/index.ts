export * from './clinique.model';
export * from './clinique-popup.service';
export * from './clinique.service';
export * from './clinique-dialog.component';
export * from './clinique-delete-dialog.component';
export * from './clinique-detail.component';
export * from './clinique.component';
export * from './clinique.route';
