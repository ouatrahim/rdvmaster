import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Clinique } from './clinique.model';
import { CliniquePopupService } from './clinique-popup.service';
import { CliniqueService } from './clinique.service';
import { Ville, VilleService } from '../ville';
import { Specialite, SpecialiteService } from '../specialite';

@Component({
    selector: 'jhi-clinique-dialog',
    templateUrl: './clinique-dialog.component.html'
})
export class CliniqueDialogComponent implements OnInit {

    clinique: Clinique;
    isSaving: boolean;

    villes: Ville[];

    specialites: Specialite[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private cliniqueService: CliniqueService,
        private villeService: VilleService,
        private specialiteService: SpecialiteService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.villeService.query()
            .subscribe((res: HttpResponse<Ville[]>) => { this.villes = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
        this.specialiteService.query()
            .subscribe((res: HttpResponse<Specialite[]>) => { this.specialites = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.clinique.id !== undefined) {
            this.subscribeToSaveResponse(
                this.cliniqueService.update(this.clinique));
        } else {
            this.subscribeToSaveResponse(
                this.cliniqueService.create(this.clinique));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Clinique>>) {
        result.subscribe((res: HttpResponse<Clinique>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Clinique) {
        this.eventManager.broadcast({ name: 'cliniqueListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackVilleById(index: number, item: Ville) {
        return item.id;
    }

    trackSpecialiteById(index: number, item: Specialite) {
        return item.id;
    }

    getSelected(selectedVals: Array<any>, option: any) {
        if (selectedVals) {
            for (let i = 0; i < selectedVals.length; i++) {
                if (option.id === selectedVals[i].id) {
                    return selectedVals[i];
                }
            }
        }
        return option;
    }
}

@Component({
    selector: 'jhi-clinique-popup',
    template: ''
})
export class CliniquePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private cliniquePopupService: CliniquePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.cliniquePopupService
                    .open(CliniqueDialogComponent as Component, params['id']);
            } else {
                this.cliniquePopupService
                    .open(CliniqueDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
