import { BaseEntity } from './../../shared';

export class Clinique implements BaseEntity {
    constructor(
        public id?: number,
        public code?: string,
        public nom?: string,
        public adresse?: string,
        public telephone?: string,
        public email?: string,
        public ville?: BaseEntity,
        public specialites?: BaseEntity[],
    ) {
    }
}
