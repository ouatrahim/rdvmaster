import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { Clinique } from './clinique.model';
import { CliniquePopupService } from './clinique-popup.service';
import { CliniqueService } from './clinique.service';

@Component({
    selector: 'jhi-clinique-delete-dialog',
    templateUrl: './clinique-delete-dialog.component.html'
})
export class CliniqueDeleteDialogComponent {

    clinique: Clinique;

    constructor(
        private cliniqueService: CliniqueService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.cliniqueService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'cliniqueListModification',
                content: 'Deleted an clinique'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-clinique-delete-popup',
    template: ''
})
export class CliniqueDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private cliniquePopupService: CliniquePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.cliniquePopupService
                .open(CliniqueDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
