import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { RdvmasterSharedModule } from '../../shared';
import {
    CliniqueService,
    CliniquePopupService,
    CliniqueComponent,
    CliniqueDetailComponent,
    CliniqueDialogComponent,
    CliniquePopupComponent,
    CliniqueDeletePopupComponent,
    CliniqueDeleteDialogComponent,
    cliniqueRoute,
    cliniquePopupRoute,
} from './';

const ENTITY_STATES = [
    ...cliniqueRoute,
    ...cliniquePopupRoute,
];

@NgModule({
    imports: [
        RdvmasterSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        CliniqueComponent,
        CliniqueDetailComponent,
        CliniqueDialogComponent,
        CliniqueDeleteDialogComponent,
        CliniquePopupComponent,
        CliniqueDeletePopupComponent,
    ],
    entryComponents: [
        CliniqueComponent,
        CliniqueDialogComponent,
        CliniquePopupComponent,
        CliniqueDeleteDialogComponent,
        CliniqueDeletePopupComponent,
    ],
    providers: [
        CliniqueService,
        CliniquePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RdvmasterCliniqueModule {}
