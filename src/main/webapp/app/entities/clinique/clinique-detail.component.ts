import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { Clinique } from './clinique.model';
import { CliniqueService } from './clinique.service';

@Component({
    selector: 'jhi-clinique-detail',
    templateUrl: './clinique-detail.component.html'
})
export class CliniqueDetailComponent implements OnInit, OnDestroy {

    clinique: Clinique;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private cliniqueService: CliniqueService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInCliniques();
    }

    load(id) {
        this.cliniqueService.find(id)
            .subscribe((cliniqueResponse: HttpResponse<Clinique>) => {
                this.clinique = cliniqueResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInCliniques() {
        this.eventSubscriber = this.eventManager.subscribe(
            'cliniqueListModification',
            (response) => this.load(this.clinique.id)
        );
    }
}
