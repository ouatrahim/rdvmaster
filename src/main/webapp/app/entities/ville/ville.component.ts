import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Ville } from './ville.model';
import { VilleService } from './ville.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-ville',
    templateUrl: './ville.component.html'
})
export class VilleComponent implements OnInit, OnDestroy {
villes: Ville[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private villeService: VilleService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.villeService.query().subscribe(
            (res: HttpResponse<Ville[]>) => {
                this.villes = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInVilles();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: Ville) {
        return item.id;
    }
    registerChangeInVilles() {
        this.eventSubscriber = this.eventManager.subscribe('villeListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
