import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { VilleComponent } from './ville.component';
import { VilleDetailComponent } from './ville-detail.component';
import { VillePopupComponent } from './ville-dialog.component';
import { VilleDeletePopupComponent } from './ville-delete-dialog.component';

export const villeRoute: Routes = [
    {
        path: 'ville',
        component: VilleComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'rdvmasterApp.ville.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'ville/:id',
        component: VilleDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'rdvmasterApp.ville.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const villePopupRoute: Routes = [
    {
        path: 'ville-new',
        component: VillePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'rdvmasterApp.ville.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'ville/:id/edit',
        component: VillePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'rdvmasterApp.ville.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'ville/:id/delete',
        component: VilleDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'rdvmasterApp.ville.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
