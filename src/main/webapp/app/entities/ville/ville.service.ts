import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { Ville } from './ville.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<Ville>;

@Injectable()
export class VilleService {

    private resourceUrl =  SERVER_API_URL + 'api/villes';

    constructor(private http: HttpClient) { }

    create(ville: Ville): Observable<EntityResponseType> {
        const copy = this.convert(ville);
        return this.http.post<Ville>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(ville: Ville): Observable<EntityResponseType> {
        const copy = this.convert(ville);
        return this.http.put<Ville>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<Ville>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<Ville[]>> {
        const options = createRequestOption(req);
        return this.http.get<Ville[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Ville[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: Ville = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<Ville[]>): HttpResponse<Ville[]> {
        const jsonResponse: Ville[] = res.body;
        const body: Ville[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to Ville.
     */
    private convertItemFromServer(ville: Ville): Ville {
        const copy: Ville = Object.assign({}, ville);
        return copy;
    }

    /**
     * Convert a Ville to a JSON which can be sent to the server.
     */
    private convert(ville: Ville): Ville {
        const copy: Ville = Object.assign({}, ville);
        return copy;
    }
}
