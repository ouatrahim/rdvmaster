import { BaseEntity } from './../../shared';

export class Ville implements BaseEntity {
    constructor(
        public id?: number,
        public code?: string,
        public nom?: string,
    ) {
    }
}
