import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { RendezVous } from './rendez-vous.model';
import { RendezVousPopupService } from './rendez-vous-popup.service';
import { RendezVousService } from './rendez-vous.service';
import { Clinique, CliniqueService } from '../clinique';

@Component({
    selector: 'jhi-rendez-vous-dialog',
    templateUrl: './rendez-vous-dialog.component.html'
})
export class RendezVousDialogComponent implements OnInit {

    rendezVous: RendezVous;
    isSaving: boolean;

    cliniques: Clinique[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private rendezVousService: RendezVousService,
        private cliniqueService: CliniqueService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.cliniqueService.query()
            .subscribe((res: HttpResponse<Clinique[]>) => { this.cliniques = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.rendezVous.id !== undefined) {
            this.subscribeToSaveResponse(
                this.rendezVousService.update(this.rendezVous));
        } else {
            this.subscribeToSaveResponse(
                this.rendezVousService.create(this.rendezVous));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<RendezVous>>) {
        result.subscribe((res: HttpResponse<RendezVous>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: RendezVous) {
        this.eventManager.broadcast({ name: 'rendezVousListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackCliniqueById(index: number, item: Clinique) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-rendez-vous-popup',
    template: ''
})
export class RendezVousPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private rendezVousPopupService: RendezVousPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.rendezVousPopupService
                    .open(RendezVousDialogComponent as Component, params['id']);
            } else {
                this.rendezVousPopupService
                    .open(RendezVousDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
