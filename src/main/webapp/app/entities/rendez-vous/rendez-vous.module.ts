import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { RdvmasterSharedModule } from '../../shared';
import {
    RendezVousService,
    RendezVousPopupService,
    RendezVousComponent,
    RendezVousDetailComponent,
    RendezVousDialogComponent,
    RendezVousPopupComponent,
    RendezVousDeletePopupComponent,
    RendezVousDeleteDialogComponent,
    rendezVousRoute,
    rendezVousPopupRoute,
} from './';

const ENTITY_STATES = [
    ...rendezVousRoute,
    ...rendezVousPopupRoute,
];

@NgModule({
    imports: [
        RdvmasterSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        RendezVousComponent,
        RendezVousDetailComponent,
        RendezVousDialogComponent,
        RendezVousDeleteDialogComponent,
        RendezVousPopupComponent,
        RendezVousDeletePopupComponent,
    ],
    entryComponents: [
        RendezVousComponent,
        RendezVousDialogComponent,
        RendezVousPopupComponent,
        RendezVousDeleteDialogComponent,
        RendezVousDeletePopupComponent,
    ],
    providers: [
        RendezVousService,
        RendezVousPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RdvmasterRendezVousModule {}
