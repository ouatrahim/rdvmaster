import { BaseEntity } from './../../shared';

export const enum StatusRdv {
    'NOUVEAU',
    'VALIDE',
    'ARCHIVE'
}

export class RendezVous implements BaseEntity {
    constructor(
        public id?: number,
        public description?: string,
        public dateCreation?: any,
        public dateDebut?: any,
        public dateFin?: any,
        public userLogin?: string,
        public status?: StatusRdv,
        public clinique?: BaseEntity,
    ) {
    }
}
