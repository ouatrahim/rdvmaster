import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { RendezVous } from './rendez-vous.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<RendezVous>;

@Injectable()
export class RendezVousService {

    private resourceUrl =  SERVER_API_URL + 'api/rendez-vous';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(rendezVous: RendezVous): Observable<EntityResponseType> {
        const copy = this.convert(rendezVous);
        return this.http.post<RendezVous>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(rendezVous: RendezVous): Observable<EntityResponseType> {
        const copy = this.convert(rendezVous);
        return this.http.put<RendezVous>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<RendezVous>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<RendezVous[]>> {
        const options = createRequestOption(req);
        return this.http.get<RendezVous[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<RendezVous[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: RendezVous = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<RendezVous[]>): HttpResponse<RendezVous[]> {
        const jsonResponse: RendezVous[] = res.body;
        const body: RendezVous[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to RendezVous.
     */
    private convertItemFromServer(rendezVous: RendezVous): RendezVous {
        const copy: RendezVous = Object.assign({}, rendezVous);
        copy.dateCreation = this.dateUtils
            .convertDateTimeFromServer(rendezVous.dateCreation);
        copy.dateDebut = this.dateUtils
            .convertDateTimeFromServer(rendezVous.dateDebut);
        copy.dateFin = this.dateUtils
            .convertDateTimeFromServer(rendezVous.dateFin);
        return copy;
    }

    /**
     * Convert a RendezVous to a JSON which can be sent to the server.
     */
    private convert(rendezVous: RendezVous): RendezVous {
        const copy: RendezVous = Object.assign({}, rendezVous);

        copy.dateCreation = this.dateUtils.toDate(rendezVous.dateCreation);

        copy.dateDebut = this.dateUtils.toDate(rendezVous.dateDebut);

        copy.dateFin = this.dateUtils.toDate(rendezVous.dateFin);
        return copy;
    }
}
