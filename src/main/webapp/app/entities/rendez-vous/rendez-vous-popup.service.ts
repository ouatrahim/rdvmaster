import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { RendezVous } from './rendez-vous.model';
import { RendezVousService } from './rendez-vous.service';

@Injectable()
export class RendezVousPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private rendezVousService: RendezVousService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.rendezVousService.find(id)
                    .subscribe((rendezVousResponse: HttpResponse<RendezVous>) => {
                        const rendezVous: RendezVous = rendezVousResponse.body;
                        rendezVous.dateCreation = this.datePipe
                            .transform(rendezVous.dateCreation, 'yyyy-MM-ddTHH:mm:ss');
                        rendezVous.dateDebut = this.datePipe
                            .transform(rendezVous.dateDebut, 'yyyy-MM-ddTHH:mm:ss');
                        rendezVous.dateFin = this.datePipe
                            .transform(rendezVous.dateFin, 'yyyy-MM-ddTHH:mm:ss');
                        this.ngbModalRef = this.rendezVousModalRef(component, rendezVous);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.rendezVousModalRef(component, new RendezVous());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    rendezVousModalRef(component: Component, rendezVous: RendezVous): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.rendezVous = rendezVous;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
