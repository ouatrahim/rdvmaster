import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { RendezVous } from './rendez-vous.model';
import { RendezVousService } from './rendez-vous.service';

@Component({
    selector: 'jhi-rendez-vous-detail',
    templateUrl: './rendez-vous-detail.component.html'
})
export class RendezVousDetailComponent implements OnInit, OnDestroy {

    rendezVous: RendezVous;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private rendezVousService: RendezVousService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInRendezVous();
    }

    load(id) {
        this.rendezVousService.find(id)
            .subscribe((rendezVousResponse: HttpResponse<RendezVous>) => {
                this.rendezVous = rendezVousResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInRendezVous() {
        this.eventSubscriber = this.eventManager.subscribe(
            'rendezVousListModification',
            (response) => this.load(this.rendezVous.id)
        );
    }
}
