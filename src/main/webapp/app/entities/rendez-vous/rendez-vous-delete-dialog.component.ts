import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { RendezVous } from './rendez-vous.model';
import { RendezVousPopupService } from './rendez-vous-popup.service';
import { RendezVousService } from './rendez-vous.service';

@Component({
    selector: 'jhi-rendez-vous-delete-dialog',
    templateUrl: './rendez-vous-delete-dialog.component.html'
})
export class RendezVousDeleteDialogComponent {

    rendezVous: RendezVous;

    constructor(
        private rendezVousService: RendezVousService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.rendezVousService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'rendezVousListModification',
                content: 'Deleted an rendezVous'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-rendez-vous-delete-popup',
    template: ''
})
export class RendezVousDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private rendezVousPopupService: RendezVousPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.rendezVousPopupService
                .open(RendezVousDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
