import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { RendezVous } from './rendez-vous.model';
import { RendezVousService } from './rendez-vous.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-rendez-vous',
    templateUrl: './rendez-vous.component.html'
})
export class RendezVousComponent implements OnInit, OnDestroy {
rendezVous: RendezVous[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private rendezVousService: RendezVousService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.rendezVousService.query().subscribe(
            (res: HttpResponse<RendezVous[]>) => {
                this.rendezVous = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInRendezVous();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: RendezVous) {
        return item.id;
    }
    registerChangeInRendezVous() {
        this.eventSubscriber = this.eventManager.subscribe('rendezVousListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
