import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { RendezVousComponent } from './rendez-vous.component';
import { RendezVousDetailComponent } from './rendez-vous-detail.component';
import { RendezVousPopupComponent } from './rendez-vous-dialog.component';
import { RendezVousDeletePopupComponent } from './rendez-vous-delete-dialog.component';

export const rendezVousRoute: Routes = [
    {
        path: 'rendez-vous',
        component: RendezVousComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'rdvmasterApp.rendezVous.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'rendez-vous/:id',
        component: RendezVousDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'rdvmasterApp.rendezVous.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const rendezVousPopupRoute: Routes = [
    {
        path: 'rendez-vous-new',
        component: RendezVousPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'rdvmasterApp.rendezVous.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'rendez-vous/:id/edit',
        component: RendezVousPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'rdvmasterApp.rendezVous.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'rendez-vous/:id/delete',
        component: RendezVousDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'rdvmasterApp.rendezVous.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
