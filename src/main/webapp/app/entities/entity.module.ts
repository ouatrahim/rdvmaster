import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { RdvmasterCliniqueModule } from './clinique/clinique.module';
import { RdvmasterVilleModule } from './ville/ville.module';
import { RdvmasterSpecialiteModule } from './specialite/specialite.module';
import { RdvmasterRendezVousModule } from './rendez-vous/rendez-vous.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        RdvmasterCliniqueModule,
        RdvmasterVilleModule,
        RdvmasterSpecialiteModule,
        RdvmasterRendezVousModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RdvmasterEntityModule {}
