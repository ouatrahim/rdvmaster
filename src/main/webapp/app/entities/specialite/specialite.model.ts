import { BaseEntity } from './../../shared';

export class Specialite implements BaseEntity {
    constructor(
        public id?: number,
        public code?: string,
        public nom?: string,
    ) {
    }
}
