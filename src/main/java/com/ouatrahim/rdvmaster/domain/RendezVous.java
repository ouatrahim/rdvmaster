package com.ouatrahim.rdvmaster.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

import com.ouatrahim.rdvmaster.domain.enumeration.StatusRdv;

/**
 * A RendezVous.
 */
@Entity
@Table(name = "rendez_vous")
public class RendezVous implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "description")
    private String description;

    @Column(name = "date_creation")
    private ZonedDateTime dateCreation;

    @Column(name = "date_debut")
    private ZonedDateTime dateDebut;

    @Column(name = "date_fin")
    private ZonedDateTime dateFin;

    @Column(name = "user_login")
    private String userLogin;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private StatusRdv status;

    @ManyToOne
    private Clinique clinique;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public RendezVous description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ZonedDateTime getDateCreation() {
        return dateCreation;
    }

    public RendezVous dateCreation(ZonedDateTime dateCreation) {
        this.dateCreation = dateCreation;
        return this;
    }

    public void setDateCreation(ZonedDateTime dateCreation) {
        this.dateCreation = dateCreation;
    }

    public ZonedDateTime getDateDebut() {
        return dateDebut;
    }

    public RendezVous dateDebut(ZonedDateTime dateDebut) {
        this.dateDebut = dateDebut;
        return this;
    }

    public void setDateDebut(ZonedDateTime dateDebut) {
        this.dateDebut = dateDebut;
    }

    public ZonedDateTime getDateFin() {
        return dateFin;
    }

    public RendezVous dateFin(ZonedDateTime dateFin) {
        this.dateFin = dateFin;
        return this;
    }

    public void setDateFin(ZonedDateTime dateFin) {
        this.dateFin = dateFin;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public RendezVous userLogin(String userLogin) {
        this.userLogin = userLogin;
        return this;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public StatusRdv getStatus() {
        return status;
    }

    public RendezVous status(StatusRdv status) {
        this.status = status;
        return this;
    }

    public void setStatus(StatusRdv status) {
        this.status = status;
    }

    public Clinique getClinique() {
        return clinique;
    }

    public RendezVous clinique(Clinique clinique) {
        this.clinique = clinique;
        return this;
    }

    public void setClinique(Clinique clinique) {
        this.clinique = clinique;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RendezVous rendezVous = (RendezVous) o;
        if (rendezVous.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), rendezVous.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "RendezVous{" +
            "id=" + getId() +
            ", description='" + getDescription() + "'" +
            ", dateCreation='" + getDateCreation() + "'" +
            ", dateDebut='" + getDateDebut() + "'" +
            ", dateFin='" + getDateFin() + "'" +
            ", userLogin='" + getUserLogin() + "'" +
            ", status='" + getStatus() + "'" +
            "}";
    }
}
