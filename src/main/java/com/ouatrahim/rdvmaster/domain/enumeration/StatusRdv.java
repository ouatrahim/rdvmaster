package com.ouatrahim.rdvmaster.domain.enumeration;

/**
 * The StatusRdv enumeration.
 */
public enum StatusRdv {
    NOUVEAU, VALIDE, ARCHIVE
}
