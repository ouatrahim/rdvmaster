/**
 * View Models used by Spring MVC REST controllers.
 */
package com.ouatrahim.rdvmaster.web.rest.vm;
