package com.ouatrahim.rdvmaster.repository;

import com.ouatrahim.rdvmaster.domain.RendezVous;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the RendezVous entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RendezVousRepository extends JpaRepository<RendezVous, Long> {

}
