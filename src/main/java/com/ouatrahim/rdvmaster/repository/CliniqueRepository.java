package com.ouatrahim.rdvmaster.repository;

import com.ouatrahim.rdvmaster.domain.Clinique;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import java.util.List;

/**
 * Spring Data JPA repository for the Clinique entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CliniqueRepository extends JpaRepository<Clinique, Long> {
    @Query("select distinct clinique from Clinique clinique left join fetch clinique.specialites")
    List<Clinique> findAllWithEagerRelationships();

    @Query("select clinique from Clinique clinique left join fetch clinique.specialites where clinique.id =:id")
    Clinique findOneWithEagerRelationships(@Param("id") Long id);

}
