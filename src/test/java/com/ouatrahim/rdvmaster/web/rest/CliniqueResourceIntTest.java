package com.ouatrahim.rdvmaster.web.rest;

import com.ouatrahim.rdvmaster.RdvmasterApp;

import com.ouatrahim.rdvmaster.domain.Clinique;
import com.ouatrahim.rdvmaster.repository.CliniqueRepository;
import com.ouatrahim.rdvmaster.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static com.ouatrahim.rdvmaster.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the CliniqueResource REST controller.
 *
 * @see CliniqueResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = RdvmasterApp.class)
public class CliniqueResourceIntTest {

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_NOM = "AAAAAAAAAA";
    private static final String UPDATED_NOM = "BBBBBBBBBB";

    private static final String DEFAULT_ADRESSE = "AAAAAAAAAA";
    private static final String UPDATED_ADRESSE = "BBBBBBBBBB";

    private static final String DEFAULT_TELEPHONE = "AAAAAAAAAA";
    private static final String UPDATED_TELEPHONE = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    @Autowired
    private CliniqueRepository cliniqueRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restCliniqueMockMvc;

    private Clinique clinique;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CliniqueResource cliniqueResource = new CliniqueResource(cliniqueRepository);
        this.restCliniqueMockMvc = MockMvcBuilders.standaloneSetup(cliniqueResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Clinique createEntity(EntityManager em) {
        Clinique clinique = new Clinique()
            .code(DEFAULT_CODE)
            .nom(DEFAULT_NOM)
            .adresse(DEFAULT_ADRESSE)
            .telephone(DEFAULT_TELEPHONE)
            .email(DEFAULT_EMAIL);
        return clinique;
    }

    @Before
    public void initTest() {
        clinique = createEntity(em);
    }

    @Test
    @Transactional
    public void createClinique() throws Exception {
        int databaseSizeBeforeCreate = cliniqueRepository.findAll().size();

        // Create the Clinique
        restCliniqueMockMvc.perform(post("/api/cliniques")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clinique)))
            .andExpect(status().isCreated());

        // Validate the Clinique in the database
        List<Clinique> cliniqueList = cliniqueRepository.findAll();
        assertThat(cliniqueList).hasSize(databaseSizeBeforeCreate + 1);
        Clinique testClinique = cliniqueList.get(cliniqueList.size() - 1);
        assertThat(testClinique.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testClinique.getNom()).isEqualTo(DEFAULT_NOM);
        assertThat(testClinique.getAdresse()).isEqualTo(DEFAULT_ADRESSE);
        assertThat(testClinique.getTelephone()).isEqualTo(DEFAULT_TELEPHONE);
        assertThat(testClinique.getEmail()).isEqualTo(DEFAULT_EMAIL);
    }

    @Test
    @Transactional
    public void createCliniqueWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = cliniqueRepository.findAll().size();

        // Create the Clinique with an existing ID
        clinique.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCliniqueMockMvc.perform(post("/api/cliniques")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clinique)))
            .andExpect(status().isBadRequest());

        // Validate the Clinique in the database
        List<Clinique> cliniqueList = cliniqueRepository.findAll();
        assertThat(cliniqueList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllCliniques() throws Exception {
        // Initialize the database
        cliniqueRepository.saveAndFlush(clinique);

        // Get all the cliniqueList
        restCliniqueMockMvc.perform(get("/api/cliniques?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(clinique.getId().intValue())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].nom").value(hasItem(DEFAULT_NOM.toString())))
            .andExpect(jsonPath("$.[*].adresse").value(hasItem(DEFAULT_ADRESSE.toString())))
            .andExpect(jsonPath("$.[*].telephone").value(hasItem(DEFAULT_TELEPHONE.toString())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())));
    }

    @Test
    @Transactional
    public void getClinique() throws Exception {
        // Initialize the database
        cliniqueRepository.saveAndFlush(clinique);

        // Get the clinique
        restCliniqueMockMvc.perform(get("/api/cliniques/{id}", clinique.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(clinique.getId().intValue()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()))
            .andExpect(jsonPath("$.nom").value(DEFAULT_NOM.toString()))
            .andExpect(jsonPath("$.adresse").value(DEFAULT_ADRESSE.toString()))
            .andExpect(jsonPath("$.telephone").value(DEFAULT_TELEPHONE.toString()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingClinique() throws Exception {
        // Get the clinique
        restCliniqueMockMvc.perform(get("/api/cliniques/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateClinique() throws Exception {
        // Initialize the database
        cliniqueRepository.saveAndFlush(clinique);
        int databaseSizeBeforeUpdate = cliniqueRepository.findAll().size();

        // Update the clinique
        Clinique updatedClinique = cliniqueRepository.findOne(clinique.getId());
        // Disconnect from session so that the updates on updatedClinique are not directly saved in db
        em.detach(updatedClinique);
        updatedClinique
            .code(UPDATED_CODE)
            .nom(UPDATED_NOM)
            .adresse(UPDATED_ADRESSE)
            .telephone(UPDATED_TELEPHONE)
            .email(UPDATED_EMAIL);

        restCliniqueMockMvc.perform(put("/api/cliniques")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedClinique)))
            .andExpect(status().isOk());

        // Validate the Clinique in the database
        List<Clinique> cliniqueList = cliniqueRepository.findAll();
        assertThat(cliniqueList).hasSize(databaseSizeBeforeUpdate);
        Clinique testClinique = cliniqueList.get(cliniqueList.size() - 1);
        assertThat(testClinique.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testClinique.getNom()).isEqualTo(UPDATED_NOM);
        assertThat(testClinique.getAdresse()).isEqualTo(UPDATED_ADRESSE);
        assertThat(testClinique.getTelephone()).isEqualTo(UPDATED_TELEPHONE);
        assertThat(testClinique.getEmail()).isEqualTo(UPDATED_EMAIL);
    }

    @Test
    @Transactional
    public void updateNonExistingClinique() throws Exception {
        int databaseSizeBeforeUpdate = cliniqueRepository.findAll().size();

        // Create the Clinique

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restCliniqueMockMvc.perform(put("/api/cliniques")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(clinique)))
            .andExpect(status().isCreated());

        // Validate the Clinique in the database
        List<Clinique> cliniqueList = cliniqueRepository.findAll();
        assertThat(cliniqueList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteClinique() throws Exception {
        // Initialize the database
        cliniqueRepository.saveAndFlush(clinique);
        int databaseSizeBeforeDelete = cliniqueRepository.findAll().size();

        // Get the clinique
        restCliniqueMockMvc.perform(delete("/api/cliniques/{id}", clinique.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Clinique> cliniqueList = cliniqueRepository.findAll();
        assertThat(cliniqueList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Clinique.class);
        Clinique clinique1 = new Clinique();
        clinique1.setId(1L);
        Clinique clinique2 = new Clinique();
        clinique2.setId(clinique1.getId());
        assertThat(clinique1).isEqualTo(clinique2);
        clinique2.setId(2L);
        assertThat(clinique1).isNotEqualTo(clinique2);
        clinique1.setId(null);
        assertThat(clinique1).isNotEqualTo(clinique2);
    }
}
