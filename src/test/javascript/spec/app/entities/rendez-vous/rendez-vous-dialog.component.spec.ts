/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { RdvmasterTestModule } from '../../../test.module';
import { RendezVousDialogComponent } from '../../../../../../main/webapp/app/entities/rendez-vous/rendez-vous-dialog.component';
import { RendezVousService } from '../../../../../../main/webapp/app/entities/rendez-vous/rendez-vous.service';
import { RendezVous } from '../../../../../../main/webapp/app/entities/rendez-vous/rendez-vous.model';
import { CliniqueService } from '../../../../../../main/webapp/app/entities/clinique';

describe('Component Tests', () => {

    describe('RendezVous Management Dialog Component', () => {
        let comp: RendezVousDialogComponent;
        let fixture: ComponentFixture<RendezVousDialogComponent>;
        let service: RendezVousService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [RdvmasterTestModule],
                declarations: [RendezVousDialogComponent],
                providers: [
                    CliniqueService,
                    RendezVousService
                ]
            })
            .overrideTemplate(RendezVousDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(RendezVousDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RendezVousService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new RendezVous(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.rendezVous = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'rendezVousListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new RendezVous();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.rendezVous = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'rendezVousListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
