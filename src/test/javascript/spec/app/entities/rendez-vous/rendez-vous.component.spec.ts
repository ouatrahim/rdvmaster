/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { RdvmasterTestModule } from '../../../test.module';
import { RendezVousComponent } from '../../../../../../main/webapp/app/entities/rendez-vous/rendez-vous.component';
import { RendezVousService } from '../../../../../../main/webapp/app/entities/rendez-vous/rendez-vous.service';
import { RendezVous } from '../../../../../../main/webapp/app/entities/rendez-vous/rendez-vous.model';

describe('Component Tests', () => {

    describe('RendezVous Management Component', () => {
        let comp: RendezVousComponent;
        let fixture: ComponentFixture<RendezVousComponent>;
        let service: RendezVousService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [RdvmasterTestModule],
                declarations: [RendezVousComponent],
                providers: [
                    RendezVousService
                ]
            })
            .overrideTemplate(RendezVousComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(RendezVousComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RendezVousService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new RendezVous(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.rendezVous[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
