/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { RdvmasterTestModule } from '../../../test.module';
import { RendezVousDetailComponent } from '../../../../../../main/webapp/app/entities/rendez-vous/rendez-vous-detail.component';
import { RendezVousService } from '../../../../../../main/webapp/app/entities/rendez-vous/rendez-vous.service';
import { RendezVous } from '../../../../../../main/webapp/app/entities/rendez-vous/rendez-vous.model';

describe('Component Tests', () => {

    describe('RendezVous Management Detail Component', () => {
        let comp: RendezVousDetailComponent;
        let fixture: ComponentFixture<RendezVousDetailComponent>;
        let service: RendezVousService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [RdvmasterTestModule],
                declarations: [RendezVousDetailComponent],
                providers: [
                    RendezVousService
                ]
            })
            .overrideTemplate(RendezVousDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(RendezVousDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(RendezVousService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new RendezVous(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.rendezVous).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
