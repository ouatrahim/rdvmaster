/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { RdvmasterTestModule } from '../../../test.module';
import { CliniqueDetailComponent } from '../../../../../../main/webapp/app/entities/clinique/clinique-detail.component';
import { CliniqueService } from '../../../../../../main/webapp/app/entities/clinique/clinique.service';
import { Clinique } from '../../../../../../main/webapp/app/entities/clinique/clinique.model';

describe('Component Tests', () => {

    describe('Clinique Management Detail Component', () => {
        let comp: CliniqueDetailComponent;
        let fixture: ComponentFixture<CliniqueDetailComponent>;
        let service: CliniqueService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [RdvmasterTestModule],
                declarations: [CliniqueDetailComponent],
                providers: [
                    CliniqueService
                ]
            })
            .overrideTemplate(CliniqueDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CliniqueDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CliniqueService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new Clinique(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.clinique).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
