/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { RdvmasterTestModule } from '../../../test.module';
import { CliniqueDialogComponent } from '../../../../../../main/webapp/app/entities/clinique/clinique-dialog.component';
import { CliniqueService } from '../../../../../../main/webapp/app/entities/clinique/clinique.service';
import { Clinique } from '../../../../../../main/webapp/app/entities/clinique/clinique.model';
import { VilleService } from '../../../../../../main/webapp/app/entities/ville';
import { SpecialiteService } from '../../../../../../main/webapp/app/entities/specialite';

describe('Component Tests', () => {

    describe('Clinique Management Dialog Component', () => {
        let comp: CliniqueDialogComponent;
        let fixture: ComponentFixture<CliniqueDialogComponent>;
        let service: CliniqueService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [RdvmasterTestModule],
                declarations: [CliniqueDialogComponent],
                providers: [
                    VilleService,
                    SpecialiteService,
                    CliniqueService
                ]
            })
            .overrideTemplate(CliniqueDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CliniqueDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CliniqueService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new Clinique(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.clinique = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'cliniqueListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new Clinique();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.clinique = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'cliniqueListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
