/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { RdvmasterTestModule } from '../../../test.module';
import { CliniqueComponent } from '../../../../../../main/webapp/app/entities/clinique/clinique.component';
import { CliniqueService } from '../../../../../../main/webapp/app/entities/clinique/clinique.service';
import { Clinique } from '../../../../../../main/webapp/app/entities/clinique/clinique.model';

describe('Component Tests', () => {

    describe('Clinique Management Component', () => {
        let comp: CliniqueComponent;
        let fixture: ComponentFixture<CliniqueComponent>;
        let service: CliniqueService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [RdvmasterTestModule],
                declarations: [CliniqueComponent],
                providers: [
                    CliniqueService
                ]
            })
            .overrideTemplate(CliniqueComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CliniqueComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CliniqueService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new Clinique(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.cliniques[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
