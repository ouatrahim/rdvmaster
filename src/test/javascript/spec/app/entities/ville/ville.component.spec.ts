/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { RdvmasterTestModule } from '../../../test.module';
import { VilleComponent } from '../../../../../../main/webapp/app/entities/ville/ville.component';
import { VilleService } from '../../../../../../main/webapp/app/entities/ville/ville.service';
import { Ville } from '../../../../../../main/webapp/app/entities/ville/ville.model';

describe('Component Tests', () => {

    describe('Ville Management Component', () => {
        let comp: VilleComponent;
        let fixture: ComponentFixture<VilleComponent>;
        let service: VilleService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [RdvmasterTestModule],
                declarations: [VilleComponent],
                providers: [
                    VilleService
                ]
            })
            .overrideTemplate(VilleComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(VilleComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(VilleService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new Ville(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.villes[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
