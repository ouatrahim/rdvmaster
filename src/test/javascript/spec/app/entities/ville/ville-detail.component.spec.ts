/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { RdvmasterTestModule } from '../../../test.module';
import { VilleDetailComponent } from '../../../../../../main/webapp/app/entities/ville/ville-detail.component';
import { VilleService } from '../../../../../../main/webapp/app/entities/ville/ville.service';
import { Ville } from '../../../../../../main/webapp/app/entities/ville/ville.model';

describe('Component Tests', () => {

    describe('Ville Management Detail Component', () => {
        let comp: VilleDetailComponent;
        let fixture: ComponentFixture<VilleDetailComponent>;
        let service: VilleService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [RdvmasterTestModule],
                declarations: [VilleDetailComponent],
                providers: [
                    VilleService
                ]
            })
            .overrideTemplate(VilleDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(VilleDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(VilleService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new Ville(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.ville).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
